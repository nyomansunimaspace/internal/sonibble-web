/**
 * This plugin use to add aos (Animation on scroll library)
 * this will use when the page scroll to the view and trigger the animation
 */

// import the aos and css file
import AOS from 'aos';
import 'aos/dist/aos.css';

// export the AOS Settings 
export default ({ app }) => {
    app.AOS = new AOS.init({
        // the settings init
        once: true,
        easing: 'ease-in-out',
        duration: 800,
    });
}