const colors = require('tailwindcss/colors');

module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      poppins: ['Poppins', 'sans-serif'],
      'work-sans': ['Work Sans', 'sans-serif'],
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: colors.white,
      gray: colors.coolGray,
      red: colors.rose,
      orange: colors.orange,
      cyan: colors.cyan,
      teal: colors.teal,
      'dive-blue': '#2036FF',
      'shine-blue': '#00EDFC',
      'extreme-red': '#F200CB',
      'blue-dark': '#35395C',
    },
    extend: {},
  },
  variants: {
    extend: {
      backgroundColor: ['active', 'checked', 'odd', 'last', 'first', 'even'],
      opacity: ['disabled'],
      borderColor: ['checked'],
      transform: ['hover', 'focus'],
    },
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
}
